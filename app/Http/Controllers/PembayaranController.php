<?php

namespace App\Http\Controllers;

use App\Models\Pembayaran;
use App\Models\User;
use App\Models\Kost;
use App\Models\Fasilitas;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use PDF;
class PembayaranController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Auth::user()->role_id == '6') {
            $pembayaran = Pembayaran::where('user_id', Auth::user()->id)
            ->Where('status_bayar', '!=', 'Diterima(Booking)')->get()->unique('tgl_bayar');
            $ids = Pembayaran::where('user_id', Auth::user()->id)->latest('tenggat')->first();
            return view('pembayaran.index', compact('pembayaran', 'ids'));
        } elseif (Auth::user()->role_id == '1') {
            $pembayaran = Pembayaran::where('status_bayar', 'Diterima')
            ->orWhere('status_bayar', 'Sudah Transfer')
            ->orwhere('status_bayar', 'Ditolak')
            ->orwhere('status_bayar', 'Menunggu Konfirmasi')->get()->unique('tgl_bayar');
            return view('pembayarans.index', ['pembayaran' => $pembayaran]);
        }
    }

    public function NotaPembayaran($id)
    {
        $no = random_int(100000, 999999);
        $nama = Auth::user()->name;
        $cek = Pembayaran::find($id);
            if (empty($cek->fas_id)) {
                $pembayaran = Pembayaran::join('kost','kost.id','=','pembayaran.kost_id')
                ->where('pembayaran.id',$cek->id)
                ->select('pembayaran.*', 'kost.nama_kost')->first();
            } else {
                $pembayaran = Pembayaran::join('kost','kost.id','=','pembayaran.kost_id')
                ->join('fasilitas','fasilitas.id','=','pembayaran.fas_id')
                ->where('pembayaran.id',$cek->id)
                ->select('pembayaran.*', 'kost.nama_kost','fasilitas.fasilitas','fasilitas.harga')->first();
            }
        $customPaper = array(0, 0,155.90551181,206.92913386);
        $pdf = PDF::loadview('pembayaran.cetak',['no' => $no, 'nama' => $nama,'pembayaran' => $pembayaran])->setPaper($customPaper,'portrait');
        return $pdf->download('Nota Pembayaran '.$nama.' Bulan '.$pembayaran->bulan.'.pdf');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $cek = Pembayaran::findorfail($id);
        if (empty($cek->fas_id)) {
            $Idfasilitas = [];
            $pembayaran =  DB::table('pembayaran')
            ->join('kost','kost.id','=','pembayaran.kost_id')
            ->where('pembayaran.id', $cek->id)
            ->select('pembayaran.*','kost.nama_kost')->first();
            $fasilitas = Fasilitas::all();
            $user = User::all();
            $kost = Kost::all();
            return view('pembayaran.create', compact('user','pembayaran','kost','fasilitas','Idfasilitas'));
        } else {
            $Idfasilitas = [];
            $pembayaran =  DB::table('pembayaran')
            ->join('fasilitas','fasilitas.id','=','pembayaran.fas_id')
            ->join('kost','kost.id','=','pembayaran.kost_id')
            ->where('pembayaran.id', $cek->id)
            ->select('pembayaran.*','fasilitas.harga','fasilitas.fasilitas','kost.nama_kost')->first();
            $fasilitas = Fasilitas::all();
            $user = User::all();
            $kost = Kost::all();
            foreach (listsFasilitas($pembayaran->tgl_bayar) as $key) {
                $Idfasilitas[] = $key->fas_id;
            }
            return view('pembayaran.create', compact('user','pembayaran','kost','fasilitas','Idfasilitas'));
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $input = $request->all();
        if (empty($input['fas_id'])) {
            $pembayaran = new Pembayaran;
            $pembayaran->user_id = $request->user_id;
            $pembayaran->kost_id = $request->kost_id;
            $pembayaran->nama_penyewa = $request->nama_penyewa;
            $pembayaran->tgl_masuk = $request->tgl_masuk;
            $pembayaran->tgl_booking = $request->tgl_booking;
            $pembayaran->status_bayar = "Menunggu Konfirmasi";
            $pembayaran->tgl_bayar = Carbon::now()->toDateString();
            $pembayaran->save();
            $pembayaran->id;
        } else {
            for($i = 0; $i < count($input['fas_id']); $i++){
                $ids[] = Pembayaran::insertGetId($request->except(['fas_id','_token','tenggat','bukti','tgl_bayar'])+[
                    'fas_id' => $input['fas_id'][$i],
                    'status_bayar' => 'Menunggu Konfirmasi',
                    'tgl_bayar' => Carbon::now()->toDateString(),
                    'created_at' => Carbon::now()->toDateTimeString()
                ]);
            }
            $pembayaran = Pembayaran::whereIn('id', $ids)->first();
        }
        return redirect()->route('pembayaran.show', $pembayaran  )->with('toast_success', 'Segera Lakukan Pembayaran!!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Pembayaran  $pembayaran
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (Auth::user()->role_id == '6') {
            $cek = Pembayaran::find($id);
            if (empty($cek->fas_id)) {
                $pembayaran = Pembayaran::join('kost','kost.id','=','pembayaran.kost_id')
                ->where('pembayaran.id',$cek->id)
                ->select('pembayaran.*', 'kost.nama_kost')->first();
            } else {
                $pembayaran = Pembayaran::join('kost','kost.id','=','pembayaran.kost_id')
                ->join('fasilitas','fasilitas.id','=','pembayaran.fas_id')
                ->where('pembayaran.id',$cek->id)
                ->select('pembayaran.*', 'kost.nama_kost','fasilitas.fasilitas','fasilitas.harga')->first();
            }
            return view('pembayaran.show', compact('pembayaran','cek'));
        } elseif (Auth::user()->role_id == '1') {
            $cek = Pembayaran::find($id);
            if (empty($cek->fas_id)) {
                $pembayaran = Pembayaran::join('kost','kost.id','=','pembayaran.kost_id')
                ->where('pembayaran.id',$cek->id)
                ->select('pembayaran.*', 'kost.nama_kost')->first();
            } else {
                $pembayaran = Pembayaran::join('kost','kost.id','=','pembayaran.kost_id')
                ->join('fasilitas','fasilitas.id','=','pembayaran.fas_id')
                ->where('pembayaran.id',$cek->id)
                ->select('pembayaran.*', 'kost.nama_kost','fasilitas.fasilitas','fasilitas.harga')->first();
            }
            return view('pembayaran.show', compact('pembayaran','cek'));
        }
    }

    public function showbulan($bln)
    {
        $pembayaran = Pembayaran::where('bulan',$bln)->paginate();
        return view('pembayarans.bulan',compact('pembayaran'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Pembayaran  $pembayaran
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $pembayaran = Pembayaran::join('fasilitas','fasilitas.id', '=', 'pembayaran.fas_id')
        ->where('pembayaran.id',$id)
        ->select('pembayaran.*', 'fasilitas.fasilitas', 'fasilitas.harga')->first();
        $user = User::all();
        $fasilitas = Fasilitas::all();
        return view('pembayaran.edit', ['pembayaran' => $pembayaran, 'user' => $user,'fasilitas' => $fasilitas]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Pembayaran  $pembayaran
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        Pembayaran::where('id', $id)
            ->update([
                'nama_penyewa' => $request->nama_penyewa,
                'tgl_bayar' => $request->tgl_bayar,
                'bulan' => $request->bulan,
                'fas_id' => $request->fasilitas

            ]);

        return redirect('/pembayaran')->with('toast_info', 'Data berhasil diubah!');
    }

    public function konfirmasi(Request $request, $id)
    {
        $images = $request->file('bukti');
        $imagebukti = 'bukti' . time() . '.' . $images->extension();
        $images->move(public_path('images'), $imagebukti);

        Pembayaran::where('id', $id)
            ->update([
                'bukti' => $imagebukti,
                'status_bayar' => $request->status

            ]);
        return redirect('/pembayaran')->with('toast_info', 'Berhasil Melakukan Pembayaran!!');
    }

    public function konfirmasiadmin(Request $request, $id)
    {
        if ($request->status == 'Ditolak') {
            Pembayaran::where('id', $id)
            ->update([
                'status_bayar' => $request->status
            ]);
            return redirect('/pembayaran')->with('toast_danger', 'Pembayaran Ditolak!');
        } elseif ($request->status == 'Diterima(Booking)') {
            Pembayaran::where('id', $id)
            ->update([
                'status_bayar' => $request->status
            ]);
            return redirect('/booking')->with('toast_success', 'Booking Diterima!');
        } elseif ($request->status == 'Ditolak(Booking)') {
            Pembayaran::where('id', $id)
            ->update([
                'status_bayar' => $request->status
            ]);
            return redirect('/booking')->with('toast_danger', 'Booking Ditolak!');
        } else {
            $tgl_bayar = Carbon::now();
            Pembayaran::where('id', $id)
            ->update([
                'status_bayar' => $request->status,
                'tgl_bayar' => $tgl_bayar->toDateString(),
                'bulan' => $tgl_bayar->isoFormat('MMMM'),
                'tenggat' => $tgl_bayar->addMonth()->toDateString()
            ]);
            return redirect('/pembayaran')->with('toast_success', 'Pembayaran Diterima!');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Pembayaran  $pembayaran
     * @return \Illuminate\Http\Response
     */
    public function destroy(Pembayaran $pembayaran)
    {
        unlink(public_path('images') . '/' . $pembayaran->bukti);

        Pembayaran::destroy($pembayaran->id);

        return redirect('/pembayaran');
    }
    public function edit_data(Pembayaran $pembayaran)
    {
        // dd($penyewa);
        return view('beranda.edit', ['pembayaran' => $pembayaran]);
    }
}
