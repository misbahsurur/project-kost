<?php

use App\Models\Pembayaran;

function listFasilitas($tgl_booking)
{
    $data = Pembayaran::join('fasilitas', 'fasilitas.id', '=', 'pembayaran.fas_id')
    ->where('pembayaran.tgl_booking', $tgl_booking)
    ->select('pembayaran.*', 'fasilitas.fasilitas','fasilitas.harga')->get();
    return $data;
}

function listsFasilitas($tgl_bayar)
{
    $data = Pembayaran::join('fasilitas', 'fasilitas.id', '=', 'pembayaran.fas_id')
    ->where('pembayaran.tgl_bayar', $tgl_bayar)
    ->select('pembayaran.*', 'fasilitas.fasilitas','fasilitas.harga')->get();
    return $data;
}

function status($status_bayar)
{
    $list = collect(['Sudah Transfer(Booking)', 'Ditolak(Booking)', 'Booking']);
    // $list->contains($status_bayar);
    $data = Pembayaran::where('status_bayar', $list->contains($status_bayar))->first();
    return $data;
}
