<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\User;

class Penyewa extends Model
{
    use HasFactory;

    protected $table = 'penyewa';

    public function user()
    {
        return $this->belongsTo(User::class);
    }

}

