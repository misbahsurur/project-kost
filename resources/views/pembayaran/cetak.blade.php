<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Nota Pembayaran</title>
    <style>
        .garis{
            border: 1px solid;
            font-size: 12px;
        }
        @page { margin: 1px; }
    </style>

</head>
<body>
    <div class="garis">
    <table>
        <tr>
            <td>
                <center>
                    <font size="3">Kost Bu Tik</font><br>
                    <font size="1"><i>Jl. Margo Tani 01 Sukorame, Kec. Mojoroto</i></font><br>
                    <font size="1"><i>Kota Kediri</i></font><br>
                    <font size="1">BCA 033278142 a.n. Suyatik</font>
                </center>
            </td>
        </tr>
    </table>
    <hr>
    <center><b>NOTA PEMBAYARAN</b></center><br>
    <table>
        @php
        $harga_kost = $pembayaran->kost->harga;
        $total_fasilitas = $total_biaya = 0;
        @endphp
        @foreach(listsFasilitas($pembayaran->tgl_bayar ) as $row)
        @php
            $total_fasilitas += $row->harga;
            $total_biaya = $harga_kost + $total_fasilitas;
        @endphp
        @endforeach
        <tr>
            <td>
                No. Pembayaran
            </td>
            <td>: {{$no}}</td>
        </tr>
        <tr>
            <td>
                Nama
            </td>
            <td>: {{$nama}}</td>
        </tr>
        <tr>
            <td>
                Kamar
            </td>
            <td>: {{$pembayaran->nama_kost}}</td>
        </tr>
        <tr>
            <td>
                Bulan
            </td>
            <td>: {{$pembayaran->bulan}}</td>
        </tr>
        <tr>
            <td>
                Fasilitas
            </td>
            @if(empty($pembayaran->fas_id))
            <td>: -</td>
            @else
            <td>: @foreach(listsFasilitas($pembayaran->tgl_bayar ) as $row)
                {{ $loop->first ? '' : ', ' }}
                {{$row->fasilitas}}
                @endforeach
            </td>
            @endif
        </tr>
        <tr>
            <td>
                Total
            </td>
            @if(empty($pembayaran->fas_id))
            <td>: Rp. {{ $harga_kost}}</td>
            @else
            <td>: Rp. {{ $total_biaya}}</td>
            @endif
        </tr>
    </table>
    </div>
</body>
</html>
